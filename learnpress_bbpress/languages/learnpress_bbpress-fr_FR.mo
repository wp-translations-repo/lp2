��    
      l       �       �   �   �      [     l     y       
   �     �  )   �  
   �  �  �  �   �     M     `     o     u  	   �     �  -   �     �   A bbPress forum will be created and connected to this course automatically. If option no discussion be chosen, all data of forum will be deleted permanently! Connect to forum Course Forum Forum Forum of course " Go back to No discussion You have to enroll the respective course! the course Project-Id-Version: LearnPress
Report-Msgid-Bugs-To: 
Last-Translator: FX Bénard <fxb@wp-translations.org>
Language-Team: French (France) (http://www.transifex.com/wp-translations/learnpress/language/fr_FR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2015-07-25 10:54+0700
PO-Revision-Date: 2017-10-05 05:40+0000
Language: fr_FR
Plural-Forms: nplurals=2; plural=(n > 1);
 Un forum bbPress sera créé et relié à ce cours automatiquement. Si l’option aucune discussion a été choisie, toutes les données du forum seront supprimées définitivement ! Connecter au forum Forum du cours Forum Forum du cours " Retour à Aucune discussion Vous devez vous inscrire au cours respectif ! le cours 