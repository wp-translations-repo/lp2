��          �       �       �   
   �           (     /  	   7  
   A     L     ^     m     t     |     �     �  �  �     f  %   |     �  	   �     �     �  !   �     �               %     -     E   Add review Average rate: <span>%.1f</span> Cancel Content Load More Loading... No review to load Please wait... Rating Reviews Title Write a Review Write a review Project-Id-Version: LearnPress
Report-Msgid-Bugs-To: 
Last-Translator: Dionizio Bonfim Bach <translations@djio.com.br>
Language-Team: Portuguese (Brazil) (http://www.transifex.com/wp-translations/learnpress/language/pt_BR/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2015-07-25 11:42+0700
PO-Revision-Date: 2017-09-23 21:28+0000
Language: pt_BR
Plural-Forms: nplurals=2; plural=(n > 1);
 Adicionar avaliação Avaliação média: <span>%.1f</span> Cancelar Conteúdo Carregar mais Carregando... Nenhuma avaliação para carregar Por favor aguarde... Classificação Avaliações Título Escreva uma avaliação Escreva uma avaliação 