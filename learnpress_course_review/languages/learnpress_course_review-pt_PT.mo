��          �       �       �   
   �           (     /  	   7  
   A     L     ^     m     t     |     �     �  �  �     _  '   r     �  	   �     �     �     �     �     �  	   
               1   Add review Average rate: <span>%.1f</span> Cancel Content Load More Loading... No review to load Please wait... Rating Reviews Title Write a Review Write a review Project-Id-Version: LearnPress
Report-Msgid-Bugs-To: 
Last-Translator: Manuela Silva <manuela.silva@sky.com>
Language-Team: Portuguese (Portugal) (http://www.transifex.com/wp-translations/learnpress/language/pt_PT/)
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2015-07-25 11:42+0700
PO-Revision-Date: 2017-09-23 21:40+0000
Language: pt_PT
Plural-Forms: nplurals=2; plural=(n != 1);
 Adicionar crítica Média das críticas: <span>%.1f</span> Cancelar Conteúdo Carregar Mais A carregar... Nenhuma crítica para carregar Por favor, aguarde... Avaliação Críticas Título Escreva uma Crítica Escreva uma crítica 